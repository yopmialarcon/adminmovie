package yops.movie.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MovieAdminApplication {

	public static void main(String[] args) {
		SpringApplication.run(MovieAdminApplication.class, args);
	}

}
