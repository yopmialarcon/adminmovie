package yops.movie.admin.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import yops.movie.admin.service.AdminMovieService;
import yops.movie.admin.service.RatingService;
import yops.movie.admin.vo.AdminMovieVO;
import yops.movie.admin.vo.ResponseVO;
import yops.movie.admin.entity.AdminMovie;
import yops.movie.admin.entity.Rating;;

@CrossOrigin(origins = "*",maxAge = 5000)
@RestController
@RequestMapping("/movie")
public class AdminMovieController {
	
	@Autowired
	private AdminMovieService adminMovieService;
	
	@Autowired
	private RatingService ratingService;
	
	@GetMapping
	public ResponseEntity<ResponseVO> listMovies(@RequestParam(name = "id", required = false) Integer id){
		ResponseVO responseVO = new ResponseVO();
		List<AdminMovie> listAdminMovie = new ArrayList<AdminMovie>();
		
		if(id == null) {
			listAdminMovie = adminMovieService.listAllMovies();
			if(listAdminMovie.isEmpty()) {
				responseVO.setCode(-1);
				responseVO.setStatus("No Content!");
				return ResponseEntity.ok(responseVO);
			}
		}else {
			AdminMovie adminMovie = new AdminMovie();
			adminMovie = adminMovieService.getMovie(id);
			if(adminMovie == null) {
				responseVO.setCode(-2);
				responseVO.setStatus("Not Found!");
				return ResponseEntity.ok(responseVO);
			}
			listAdminMovie.add(adminMovie);
		}
		
		responseVO.setCode(0);
		responseVO.setStatus("Success!!");
		responseVO.setSearch(listAdminMovie);
		return ResponseEntity.ok(responseVO);
	}
	
	@GetMapping("/imdbid")
	public ResponseEntity<ResponseVO> listMoviesByImdbid(@RequestParam(name = "i") String imdbid){
		ResponseVO responseVO = new ResponseVO();
		List<AdminMovie> listAdminMovie = new ArrayList<AdminMovie>();
		
		
			AdminMovie adminMovie = new AdminMovie();
			adminMovie = adminMovieService.getMovieByImdbid(imdbid);
			if(adminMovie == null) {
				responseVO.setCode(-2);
				responseVO.setStatus("Not Found!");
				return ResponseEntity.ok(responseVO);
			}
			listAdminMovie.add(adminMovie);
		
		
		responseVO.setCode(0);
		responseVO.setStatus("Success!!");
		responseVO.setSearch(listAdminMovie);
		return ResponseEntity.ok(responseVO);
	}
	
	@PostMapping
	public ResponseEntity<ResponseVO> createMovie(@Valid @RequestBody AdminMovieVO movieVO ,BindingResult result){
		List<AdminMovie> listMovie = new ArrayList<AdminMovie>();
		List<Rating> listRatingCreate = new ArrayList<Rating>();
		ResponseVO responseVO = new ResponseVO();
		if(result.hasErrors()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, this.formatMessages(result));
		}
		String imdbID = adminMovieService.getImdbAPI(movieVO.getTittle());
		
		if(imdbID.equals("")) {
			responseVO.setCode(-3);
			responseVO.setStatus("Not Found on omdb!");
			
			return ResponseEntity.ok(responseVO); 
		}
		movieVO.getMovie().setImdbid(imdbID);
		List<Rating> listRating = movieVO.getMovie().getRating();
		movieVO.getMovie().setRating(null);
		AdminMovie movieCreate = adminMovieService.createdMovie(movieVO.getMovie());
		
		if(movieCreate != null ) {
			
			for (Rating rating : listRating) {
				rating.setRatingId(movieCreate.getId());
				
				Rating ratingCreate = ratingService.createRating(rating);
				listRatingCreate.add(ratingCreate);
			}
			
			
		}
		movieCreate.setRating(listRatingCreate);
		listMovie.add(movieCreate);
		responseVO.setCode(0);
		responseVO.setStatus("Success save!!");
		responseVO.setSearch(listMovie);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(responseVO);
	}
	
	@PutMapping(value = "/{id}")
	public ResponseEntity<ResponseVO> updateMovie(@PathVariable("id") Integer id, @RequestBody AdminMovieVO movieVO){
		List<AdminMovie> listMovie = new ArrayList<AdminMovie>();
		List<Rating> listRatingUpdate = new ArrayList<Rating>();
		ResponseVO responseVO = new ResponseVO();
		
		movieVO.getMovie().setId(id);
		List<Rating> listRating = movieVO.getMovie().getRating();
		AdminMovie movieBD = adminMovieService.updateMovie(movieVO.getMovie());
		
		
		
		if(movieBD == null) {
			responseVO.setCode(-2);
			responseVO.setStatus("Not Found!");
			return ResponseEntity.ok(responseVO);
		}else {
			for (int i = 0; i < listRating.size(); i++) {
				listRating.get(i).setId(movieBD.getRating().get(i).getId());
				listRating.get(i).setRatingId(movieBD.getRating().get(i).getRatingId());
				
				Rating ratingBD = ratingService.updateRating(listRating.get(i));
				if(ratingBD == null) {
					responseVO.setCode(-2);
					responseVO.setStatus("Not Found Rating! "+ listRating.get(i).getId());
					return ResponseEntity.ok(responseVO);
				}
				listRatingUpdate.add(ratingBD);
			}
		}
		movieBD.setRating(listRatingUpdate);
		listMovie.add(movieBD);
		responseVO.setCode(0);
		responseVO.setStatus("Success Update!!");
		responseVO.setSearch(listMovie);
		return ResponseEntity.ok(responseVO);
		
	}
	
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<ResponseVO> deleteMovie(@PathVariable("id") Integer id){
		List<AdminMovie> listMovie = new ArrayList<AdminMovie>();
		List<Rating> listRatingDelete = new ArrayList<Rating>();
		ResponseVO responseVO = new ResponseVO();
		
		AdminMovie movieDelete = adminMovieService.deleteMovie(id);
		List<Rating> listRating = new ArrayList<Rating>();
		if(movieDelete == null || movieDelete.getRating() == null) {
			responseVO.setCode(-2);
			responseVO.setStatus("Not Found!");
			return ResponseEntity.ok(responseVO);
		}else {
			listRating = movieDelete.getRating();
			for (Rating rating : listRating) {
				Rating ratingDelete = ratingService.deleteRating(rating.getId());
				if(ratingDelete == null) {
					responseVO.setCode(-2);
					responseVO.setStatus("Not Found Rating! "+ rating.getId());
					return ResponseEntity.ok(responseVO);
				}
				listRatingDelete.add(ratingDelete);
			}
			
		}
		movieDelete.setRating(listRatingDelete);
		listMovie.add(movieDelete);
		responseVO.setCode(0);
		responseVO.setStatus("Success Delete!!");
		responseVO.setSearch(listMovie);
		return ResponseEntity.ok(responseVO);
	}
	
	private String formatMessages(BindingResult result) {
		List<Map<String,String>> errors = result.getFieldErrors().stream()
				.map(err ->{
                    Map<String,String>  error =  new HashMap<>();
                    error.put(err.getField(), err.getDefaultMessage());
                    return error;

                }).collect(Collectors.toList());
		
		 ErrorMessage errorMessage = ErrorMessage.builder()
	                .code("01")
	                .messages(errors).build();
	        ObjectMapper mapper = new ObjectMapper();
	        String jsonString="";
	        try {
	            jsonString = mapper.writeValueAsString(errorMessage);
	        } catch (JsonProcessingException e) {
	            e.printStackTrace();
	        }
	        return jsonString;
	}
}
