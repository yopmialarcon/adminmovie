package yops.movie.admin.controller;

import java.util.List;
import java.util.Map;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter @Getter @Builder
public class ErrorMessage {
	
	private String code;
	
	private List<Map<String,String>> messages;

}
