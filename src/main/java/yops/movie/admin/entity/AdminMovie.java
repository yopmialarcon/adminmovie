package yops.movie.admin.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "ADMIN_MOVIE")
@Data @AllArgsConstructor @NoArgsConstructor @Builder
public class AdminMovie {
	
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name="RATING")
	@OneToMany(fetch = FetchType.EAGER)
	@JoinColumn(name="RATING_ID")
	@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
	private List<Rating> rating;
	
	@Column(name="FECHA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecha;
	
	@Column(name="COMENTARIOS")
	@NotEmpty(message = "Los comentarios no pueden ir vacios")
	private String comentarios;
	
	@Column(name="IMDBID")
	private String imdbid;

}
