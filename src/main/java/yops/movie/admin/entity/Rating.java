package yops.movie.admin.entity;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "RATING")
@Data @AllArgsConstructor @NoArgsConstructor @Builder
public class Rating {
	
	@Id
	@Column(name="ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name="RATING_ID")
	@PositiveOrZero(message = "El ratingId no puede ser negativo") 
	@NotNull(message ="El ratingId no puede ser nulo")
	private int ratingId;

	@Column(name="SOURCE")
	@NotEmpty(message = "El source no puede ir vacio")
	private String source;
	
	@Column(name="VALUE")
	@NotEmpty(message = "El valor no puede ir vacio")
	private String value;
}
