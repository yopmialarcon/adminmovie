package yops.movie.admin.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import yops.movie.admin.entity.AdminMovie;

public interface AdminMovieRepository extends JpaRepository<AdminMovie, Integer>{
	
	public AdminMovie findByImdbid(String imdbid);
}
