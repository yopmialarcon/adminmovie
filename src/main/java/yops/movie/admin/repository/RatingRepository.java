package yops.movie.admin.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import yops.movie.admin.entity.Rating;


public interface RatingRepository extends JpaRepository<Rating, Integer> {

}
