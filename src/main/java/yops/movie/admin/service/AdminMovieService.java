package yops.movie.admin.service;

import java.util.List;

import yops.movie.admin.entity.AdminMovie;

public interface AdminMovieService {

	public List<AdminMovie> listAllMovies();

	public AdminMovie getMovie(Integer id);

	public AdminMovie createdMovie(AdminMovie movie);

	public AdminMovie updateMovie(AdminMovie movie);

	public AdminMovie deleteMovie(Integer id);

	public AdminMovie getMovieByImdbid(String imdbid);

	public String getImdbAPI(String tittle);
}
