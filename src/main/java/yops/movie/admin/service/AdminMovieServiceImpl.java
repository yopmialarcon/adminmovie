package yops.movie.admin.service;

import java.util.List;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONObject;  
import org.json.JSONArray; 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import yops.movie.admin.entity.AdminMovie;
import yops.movie.admin.repository.AdminMovieRepository;

@Service
@RequiredArgsConstructor
public class AdminMovieServiceImpl implements AdminMovieService{
	
	@Autowired
	private final AdminMovieRepository adminMovieRepository;

	@Override
	public List<AdminMovie> listAllMovies() {
		
		return adminMovieRepository.findAll();
	}

	@Override
	public AdminMovie getMovie(Integer id) {
		
		return adminMovieRepository.findById(id).orElse(null);
	}

	@Override
	public AdminMovie createdMovie(AdminMovie movie) {

		return adminMovieRepository.save(movie);
	}

	@Override
	public AdminMovie updateMovie(AdminMovie movie) {
		AdminMovie movieBD = getMovie(movie.getId());
		
		if(movieBD == null) {
			return null;
		}
		
		movieBD.setFecha(movie.getFecha());
		movieBD.setComentarios(movie.getComentarios());
		
		return adminMovieRepository.save(movieBD);
	}

	@Override
	public AdminMovie deleteMovie(Integer id) {
		AdminMovie movieBD = getMovie(id);
		
		if(movieBD == null) {
			return null;
		}
		
		adminMovieRepository.deleteById(id);
		
		return movieBD;
	}

	@Override
	public AdminMovie getMovieByImdbid(String imdbid) {

		return adminMovieRepository.findByImdbid(imdbid);
	}

	@Override
	public String getImdbAPI(String tittle) {
		String imdbID = "";
        try {           
            URL url = new URL("http://www.omdbapi.com/?t="+tittle+"&apikey=5a309b46");//your url i.e fetch data from .
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP Error code : "
                        + conn.getResponseCode());
            }
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String output;
            if ((output = br.readLine()) != null) {
                JSONArray array = new JSONArray("[" + output + "]");
                for (int i = 0; i < array.length(); i++) {
                    JSONObject object = array.getJSONObject(i);
                    imdbID = object.getString("imdbID");
                }
            }          
            conn.disconnect();

        } catch (Exception e) {
            System.out.println("Exception in NetClientGet:- " + e);
        }
        
        return imdbID;
	}

}
