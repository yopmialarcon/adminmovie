package yops.movie.admin.service;

import yops.movie.admin.entity.Rating;

public interface RatingService {

	public Rating createRating(Rating rating);

	public Rating updateRating(Rating rating);

	public Rating deleteRating(int id);

}
