package yops.movie.admin.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import yops.movie.admin.entity.AdminMovie;
import yops.movie.admin.entity.Rating;
import yops.movie.admin.repository.AdminMovieRepository;
import yops.movie.admin.repository.RatingRepository;

@Service
@RequiredArgsConstructor
public class RatingServiceImpl implements RatingService{
	
	@Autowired
	private final RatingRepository ratingRepository;

	@Override
	public Rating createRating(Rating rating) {
		
		return ratingRepository.save(rating);
	}

	@Override
	public Rating updateRating(Rating rating) {
				
		return ratingRepository.save(rating);
	}

	@Override
	public Rating deleteRating(int id) {
		Rating ratingBD = ratingRepository.findById(id).orElse(null);
		
		if(ratingBD == null) {
			return null;
		}
		
		ratingRepository.deleteById(id);
		
		return ratingBD;
	}

}
