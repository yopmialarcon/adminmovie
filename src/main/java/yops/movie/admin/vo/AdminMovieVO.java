package yops.movie.admin.vo;


import lombok.Data;
import yops.movie.admin.entity.AdminMovie;

@Data
public class AdminMovieVO {

	private String tittle;
	
	private AdminMovie movie;
}
