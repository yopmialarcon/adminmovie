package yops.movie.admin.vo;

import java.util.List;

import lombok.Data;
import yops.movie.admin.entity.AdminMovie;

@Data
public class ResponseVO {
	
	private int code;
	
	private String status;
	
	private List<AdminMovie> search;

}
