package yops.movie.admin;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import yops.movie.admin.entity.AdminMovie;
import yops.movie.admin.entity.Rating;
import yops.movie.admin.repository.AdminMovieRepository;
import yops.movie.admin.repository.RatingRepository;
import yops.movie.admin.service.AdminMovieService;
import yops.movie.admin.service.AdminMovieServiceImpl;

@SpringBootTest
class MovieAdminApplicationTests {

	@Mock
	private AdminMovieRepository adminMovieRepository;
	
	@Mock
	private RatingRepository ratingRepository;
	
	private AdminMovieService adminMovieService;
	
	@BeforeEach
	public void setup() {
		
		List<AdminMovie> listMovie = new ArrayList<AdminMovie>();
		List<Rating> listRating = new ArrayList<Rating>();
		MockitoAnnotations.initMocks(getClass());
		adminMovieService = new AdminMovieServiceImpl(adminMovieRepository);
		
		Rating rating1 = Rating.builder()
				.id(0)
				.source("INTENET MOVIE DATABASE")
				.value("10/10").build();
		listRating.add(rating1);
		Rating rating2 = Rating.builder()
				.id(1)				
				.source("ROTTEN TOMATOES")
				.value("90%").build();
		listRating.add(rating2);
		Rating rating3 = Rating.builder()
				.id(2)
				.source("METACRITIC")
				.value("77/100").build();
		listRating.add(rating3);
		AdminMovie movie = AdminMovie.builder()
				.id(0)
				.fecha(new Date())
				.comentarios("BUENA PELICULA PARA PALOMEAR")
				.imdbid("t653829393").build();
		
		AdminMovie movie1 = AdminMovie.builder()
				.id(1)
				.fecha(new Date())
				.comentarios("PESIMA PELICULA")
				.imdbid("tt4952446").build();
		
		
		listMovie.add(movie);
		listMovie.add(movie1);
		
		for (AdminMovie adminMovie : listMovie) {
			Mockito.when(adminMovieRepository.save(adminMovie)).thenReturn(adminMovie);
		}
		
		for (Rating rating : listRating) {
			Mockito.when(ratingRepository.save(rating)).thenReturn(rating);
		}
		
		Mockito.when(adminMovieRepository.findById(0)).thenReturn(Optional.of(movie));
		Mockito.when(adminMovieRepository.findById(1)).thenReturn(Optional.of(movie1));
		Mockito.when(adminMovieRepository.findAll()).thenReturn(listMovie);
		
	}
	
	@Test
	public void whenValGetId_thenReturnMovie() {
		
		AdminMovie found = adminMovieService.getMovie(0);
		Assertions.assertThat(found.getId()).isEqualTo(0);
		
		List<AdminMovie> listMovie = adminMovieService.listAllMovies();
		Assertions.assertThat(listMovie).isNotEmpty();
	}
	
	@Test
	public void when_thenReturnAllMovies() {
				
		List<AdminMovie> listMovie = adminMovieService.listAllMovies();
		Assertions.assertThat(listMovie).isNotEmpty();
	}
	
	@Test
	public void whenValidUpdateMovie_thenReturnUpdateMovie() {
		AdminMovie adminMovie = new AdminMovie();
		adminMovie.setId(0);
		adminMovie.setFecha(new Date());
		adminMovie.setComentarios("PELICULA MUY MALA");
		adminMovie.setImdbid("t653829393");
		AdminMovie updateMovie = adminMovieService.updateMovie(adminMovie);
		
		Assertions.assertThat(updateMovie.getImdbid()).isEqualTo("t653829393");
	}
	
	@Test
	public void whenValidDeleteMovie_thenReturnDeleteMovie() {
		AdminMovie deleteMovie = adminMovieService.deleteMovie(1);
		Assertions.assertThat(deleteMovie.getImdbid()).isEqualTo("tt4952446");
	}

}
